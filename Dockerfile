FROM php:7.4-fpm-buster
ARG GROUP_ID=501
ARG USER_ID=48

RUN apt-get update && apt-get install -y \
  cron \
  git \
  gzip \
  libbz2-dev \
  libfreetype6-dev \
  libicu-dev \
  libjpeg62-turbo-dev \
  libmcrypt-dev \
  libpng-dev \
  libsodium-dev \
  libssh2-1-dev \
  libxslt1-dev \
  libzip-dev \
  lsof \
  default-mysql-client \
  vim \
  zip

RUN docker-php-ext-configure gd --with-freetype --with-jpeg

RUN docker-php-ext-install \
  bcmath \
  bz2 \
  calendar \
  exif \
  gd \
  gettext \
  intl

RUN apt-get install -y libonig-dev

RUN docker-php-ext-install \
  mysqli \
  opcache \
  pcntl \
  pdo_mysql \
  soap \
  sockets \
  sodium \
  sysvmsg \
  sysvsem \
  sysvshm \
  xsl \
  zip

RUN pecl channel-update pecl.php.net \
  && pecl install xdebug

RUN docker-php-ext-enable xdebug \
  && sed -i -e 's/^zend_extension/\;zend_extension/g' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN apt-get install -y gnupg

RUN curl -sS https://getcomposer.org/installer | \
  php -- --version=1.9.0 --install-dir=/usr/local/bin --filename=composer

RUN groupadd -g $GROUP_ID opsworks \
 && useradd -g $GROUP_ID -u $USER_ID -d /var/www -s /bin/bash apache
RUN export COMPOSER_ALLOW_SUPERUSER=1

RUN mkdir -p /srv/www/magento/current
RUN mkdir -p /var/log/php-fpm
RUN touch /var/log/php-fpm/access.log
RUN touch /var/log/php-fpm/error.log
RUN chown apache:opsworks -R /var/log/php-fpm

COPY ./www.conf /usr/local/etc/php-fpm.d/
COPY ./php.ini /usr/local/etc/php/

WORKDIR /srv/www/magento/current

USER apache:opsworks